import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.cm as cm
from numpy import linalg as LA

k1 = 1.0
k2 = 1.0
k3 = 1.0

m1 = 1.0
m2 = 1.0

c1 = 0.5
c2 = 0.5
c3 = 0.5

#Not actually doing damping
print("\n")
#print("||===== c1 =====|----|===== c2 =====|----|===== c3 =====||")
print("||              |----|              |----|              ||")
print("||/\/\/ k1 \/\/\|-m1-|/\/\/ k2 \/\/\|-m2-|/\/\/ k3 \/\/\||")
print("||              |----|              |----|              ||")
#print("<------- x1 ------><------- x2 -------><----- x3 ------>||")
#print("||/\/\/ k1 \/\/\|----|/\/\/ k2 \/\/\|----|/\/\/ k3 \/\/\||")
print("\n")

print("Please enter the values for the variables in the 2 DoF Mass-Spring-Damper System Shown Above")


k1 = float(input("k1: "))
k2 = float(input("k2: "))
k3 = float(input("k3: "))
print("\n")
m1 = float(input("m1: "))
m2 = float(input("m2: "))
print("\n")

#c1 = float(input("c1: "))
#c2 = float(input("c2: "))
#c3 = float(input("c3: "))
#print("\n")


M = np.matrix(	[[m1, 0.0], 
				 [0.0, m2]])

# C = np.matrix(	[ [c1 + c2, -c2],
# 				  [-c2, c2 + c3] ])

K = np.matrix(	[ [k1 + k2, -k2],
				  [-k2, k2 + k3] ])

A = LA.inv(M) * K * -1

#A = lambda * X

eig = LA.eig(A)

print ("A: \n{0}".format(A))
print ("\n")

print ("Natural Frequencies:")

for i,j in enumerate(eig[0]):
	print ("f{0} = {1}".format(i, np.sqrt(np.fabs(j)) / (np.pi * 2)))

print ("\n")

v = []
print ("Mode Shapes:")
for i,j in enumerate(eig[1]):
	v.append(j)
	print ("v{0} = ({1}, {2})".format(i, j[0,0], j[0,1]))

x1 = float(input("Initial Displacement of m1: "))
x2 = float(input("Initial Displacement of m2: "))
print("\n")

V = np.matrix([[v[0][0,0], v[1][0,0]],[v[0][0,1], v[1][0,1]]])
#x1 = 1
#x2 = 1
X0 = np.matrix([[x1],[x2]])
gamma =  V.T * X0

#print("X0={0}".format(X0) )
#print ("V={0}".format(V))
#print ("gamma = {0}".format(gamma)) 

print("\n")

print("Starting Animation...")

fig = plt.figure()

xmin = 0
xmax = 15

ax = plt.axes(xlim=( xmin -1, xmax + 1) , ylim=(-2, 2))
line, = ax.plot([], [], lw=2, marker="D", markersize=10)
timestep = 0.1

#Equilibrium positions
p01 = 5
p02 = 10

def get_mass_positions(t):
	v1 = np.matrix([[v[0][0,0]], [v[0][0,1]]])
	v2 = np.matrix([[v[1][0,0]], [v[1][0,1]]])

	g1 = gamma[0][0,0]
	g2 = gamma[1][0,0]

	omega1 = np.sqrt(np.fabs(eig[0][0]))
	omega2 = np.sqrt(np.fabs(eig[0][1]))
	
	return (g1 * v1 * np.cos(omega1 * t)) + (g2 * v2 * np.cos(omega2 * t))

def init():
	line.set_data([], [])
	return [line]

def animate(i):
	p = get_mass_positions(i * timestep)
	P1 = xmin + p01 + p[0,0]
	P2  = xmin + p02 + p[1,0]
	line.set_data( [xmin, P1, P2, xmax], [0, 0, 0, 0])
	return tuple([line])

anim = animation.FuncAnimation(fig, animate, init_func=init, frames=1000, interval=50, blit=True)
plt.show()
#Make an animation of mass positions over time
